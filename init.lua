-- INDEX
-- lua/options
-- lua/plugins
-- lua/treesitter
-- lua/lspconfig
-- lua/toggleterm
-- lua/keymap

-- NeoVim options
require('options')

-- Load plugins
require('plugins')

-- Tree Sitter
require('treesitter')

-- LSP Config
require('lspconfig')

-- Toggle Term
--require('toggleterm')

-- Keymap
require('keymap')

