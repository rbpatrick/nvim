-- Libraries
local map = require("utils").map

-- Vim specific
map("n", "<leader>e", ":e $MYVIMRC<cr>")

-- Telescope
map("n", "<leader><leader>", ":lua require('telescope.builtin').find_files()<cr>")
map("n", "<leader>fg", ":lua require('telescope.builtin').live_grep()<cr>")
map("n", "<leader>fb", ":lua require('telescope.builtin').buffers()<cr>")
map("n", "<leader>fh", ":lua require('telescope.builtin').help_tags()<cr>")
