-- Visual
vim.wo.number = true            -- Show line numbers
vim.o.termguicolors = true      -- Better GUI colors
vim.o.cmdheight = 1             -- Better Error Messages
vim.o.showtabline = 2 	        -- Always Show Tabline
vim.o.pumheight = 10            -- Pop up Menu Height
vim.o.title = true              -- Display File Info on Title
vim.cmd [[colorscheme nord]]    -- Color Scheme

-- Behavior
vim.o.hlsearch = false 	    -- Set highlight on search
vim.o.ignorecase = true     -- Case insensitive searching 
vim.o.smartcase = true      -- If Upper Case Char > case sensitive search
vim.o.smarttab = true       -- Smart Tabs
vim.o.smartindent = true    -- Smart Indenting
vim.o.expandtab = true      -- Tabs into spaces
vim.o.tabstop = 4           -- Tabstop 
vim.o.softtabstop = 4       -- Number of spaces that <TAB> uses while editing
vim.o.shiftwidth = 4        -- Number of spaces to use for (auto)indent step

-- Vim specific
vim.o.hidden = true                     -- Do not save when switching buffers
vim.o.breakindent = true	            -- Enable break indent
vim.o.backup = false	                -- Disable Backup
vim.o.swapfile = false	                -- Don't create Swap Files
vim.o.spell = false                     -- Spell checking
vim.o.undofile = true                   -- Save undo history
vim.o.updatetime = 250	                -- Decrease update time
vim.o.timeoutlen = 250	                -- Time for mapped sequence to complete (in ms)
vim.o.inccommand = 'nosplit'            -- Incremental live completion
vim.o.fileencoding = "utf-8"            -- Set File Encoding
vim.o.spelllang = "en"                  -- Set spelling language
vim.o.completeopt = "menuone,noselect"  -- Autocompletion
vim.g.mapleader = " "                   -- Map <leader> to spacebar
vim.g.netrw_banner = 0                  -- Remove banner from Netrw

