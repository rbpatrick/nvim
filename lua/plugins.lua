return require('packer').startup(function()
  
    -- Packer
    use 'wbthomason/packer.nvim'

    -- Nord theme
    use { 'arcticicestudio/nord-vim' }

    -- Status line
    use { 'itchyny/lightline.vim' }

    -- Toggle Term
    use {"akinsho/toggleterm.nvim"}

    -- TreeSitter (Highlighting, Indentation, Folding)
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    }

    -- NeoVim lspconfig
    --use { 'neovim/nvim-lspconfig' }

    -- ripgrep
    use { 'BurntSushi/ripgrep' }

    -- fd
    use { 'sharkdp/fd' }

    -- Telescope (file search)
    use {
        'nvim-telescope/telescope.nvim',
        requires = {{'nvim-lua/plenary.nvim'}, {'BurntSushi/ripgrep'}, {'sharkdp/fd'}}
    }

end)


